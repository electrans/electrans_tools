from trytond.pool import Pool


def generate_line_positions(lines):
    SaleLine = Pool().get('sale.line')
    separator = '.'
    pos = [0, 0, 0]
    level = 0
    for line in lines:
        if isinstance(line, SaleLine) and line.parent:
            line.position = line.parent.position
            continue
        if line.type == 'title':
            pos = [pos[0] + 1, 0, 0]
            level = 1
        elif line.type == 'subtitle':
            pos[1:2] = [pos[1] + 1, 0]
            level = 2
        elif line.type == 'subtotal':
            level = 0
        elif line.type == 'subsubtotal':
            level = 1
        elif line.type == 'line':
            pos[level] = pos[level] + 1

        if line.type in ('line', 'title', 'subtitle'):
            line.position = str(pos[0]) + separator if not pos[1] \
                else str(pos[0]) + separator + str(pos[1]) + separator if not pos[2] \
                else str(pos[0]) + separator + str(pos[1]) + separator + str(pos[2]) + separator
